import {combineReducers,applyMiddleware} from 'redux'
import Cart from './Cart'
import Product from './Product'
export default combineReducers({
    Cart,
    Product
})
