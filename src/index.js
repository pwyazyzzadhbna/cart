import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
/*


"cloth": [{
    "t_shirt":[

   { "id": 1, "title": "male","kind":"t_shirt", "color": "red","img":"https://i.pinimg.com/originals/d5/b0/ba/d5b0ba6f029f142c2ff8358ea8baca2d.png","price":1000000 },
   { "id": 2, "title": "male","kind":"t_shirt", "color": "yellow","img":"https://5.imimg.com/data5/FW/GT/MY-23375112/men-s-yellow-color-t-shirt-250x250.jpg","price":1000000 }
     ],
     "Coat":[
         { "id": 1, "title": "male", "kind": "palto","img":"https://images-dynamic-arcteryx.imgix.net/S21/1350x1710/Keppel-Trench-Coat-Megacosm.jpg?auto=format&w=1350","price":1000000 }
   ,
   { "id": 2, "title": "male", "kind": "palto","img":"https://www.schottnyc.com/images/1300x2000/740_NAV_frt1.jpg","price":1000000 }

         ,
         { "id": 1, "title": "female", "kind": "palto","img":"https://en.louisvuitton.com/images/is/image/lv/1/PP_VP_L/louis-vuitton-giant-monogram-jacquard-wrap-coat-in-camel-ready-to-wear--FKCO13NUV130_PM2_Front%20view.jpg","price":1000000 }
             ,
         { "id": 2, "title": "female", "kind": "palto","img":"https://imagescdn.simons.ca/images/12562-205246-36-A1_2/draped-tie-coat.jpg?__=6","price":1000000 }
     ]
          ,


*/