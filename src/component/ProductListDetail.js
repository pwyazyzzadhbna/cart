import React from 'react'
export default function ProductListDetail({value,onAddToCart}){
    return(
        <>
           <div  className="card" style={{width:"400px"}}>
            <img className="card-img-top container"  src={value.img} alt={value.title} key={value.id} style={{width:"350px",height:"350px"}}/>
  <div className="card-body">
    <h4 className="card-title">{value.kind}</h4>
    <p className="card-text">price:{value.price}$ && {value.inventory}</p>
    <button className="btn btn-primary"
    disabled={value.inventory>0?'':"disabled"}
    onClick={onAddToCart}
    >{value.inventory>0?'اضافه به  سبد خرید':"محصول تمام شده است"}
    
    </button>
  </div>
       </div>
        </>
    )
}