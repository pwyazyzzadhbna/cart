import React,{useState,useEffect}from 'react'
import axios from 'axios'
import{connect, useDispatch}from 'react-redux'
import {recivedProducts,addToCart}from '../actions/index'
import ProductListDetail from './ProductListDetail'
import Carousel from 'react-bootstrap/Carousel'
function Data(props){
    const dispatch=useDispatch()
    const [item,setItem]=useState([])
useEffect(()=>{
async function fetchDeta(){
    const response=await axios("http://localhost:3000/posts")
    dispatch(recivedProducts(response.data))
    setItem(response.data)
    console.log(response)
}
fetchDeta()
},[])

console.log(props.prouduct)
return(
    <>

  <div className="container" style={{display:"flex",flexFlow:"row wrap"}}>
    {props.prouduct?.map((value)=>{
        
        return(
            <>
            <ProductListDetail key={value.id} value={value} onAddToCart={()=>dispatch(addToCart(value.id))}/>
            </>
        )
    })}
    </div>
    </>
)
}
const mapStateToProps=(state)=>({
        prouduct:state.Product
    })

export default connect(mapStateToProps)(Data)
/*
  
*/