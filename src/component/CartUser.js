import React,{useEffect} from 'react'
import{connect, useDispatch}from 'react-redux'
import Button from 'react-bootstrap/Button'

function CartUser ({Product}){
    
    return(
        <>
        <div style={{display:"flex",flexWrap:'wrap',alignContent:"spaceBetween",alignItems:"center",justifyContent:"center"}}>
       {Product.length?(<>{Product?.map(value=>{
           return(
               <div style={{display:"flex",FlexDirection:"column"}}   key={value.id}><br/>
                   <img style={{width:"100px",height:"150px"}} src={value.img}/>
                   <span>{value.kind}</span>
               </div>
           )
       })}<Button>تصفیه حساب</Button></>):<h1>شما محصولی سفارش ندادید</h1>}
       </div>
        </>
    )
}


export default CartUser